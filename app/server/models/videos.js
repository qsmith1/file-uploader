var mongoose = require('mongoose');

var videosSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: 'Title cannot be blank'
    },
    videoName: {
        type: String
    },
    mimeType: {
        type: String
    },
    size: {
        type: String
    }, 
    duration: {
        type: Number
    }, 
    path: {
        type: String
    }
});

module.exports = mongoose.model('Video', videosSchema);
