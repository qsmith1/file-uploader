var mongoose = require('mongoose');

var PlaylistSchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: String
  },
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Name cannot be blank'
  },
  duration: {
    type: mongoose.Schema.Types.Number,
    default: 0,
    ref: 'Video'
  },
  videos: [{ 
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Video' 
  }]
    
});

module.exports = mongoose.model('Playlist', PlaylistSchema);