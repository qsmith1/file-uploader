exports.getDurationSeconds = (durationStr) => {
  if (!durationStr) {
      return null;
  }
  let a = durationStr.split(':'); 
  let seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 

  return seconds;
}

exports.getSize = (size) => {
    if (size > Math.pow(10,9)) {
        return (size/Math.pow(10,9)).toFixed(2) + ' GB';
    } else if (size > Math.pow(10,6)) {
        return (size/Math.pow(10,6)).toFixed(2) + ' MB';
    } else if (size > 1000) {
        return (size/1000).toFixed(2) + ' KB';
    } 
    return size;
}