var fs = require('fs');
var ffmpeg = require('ffmpeg');
const { postToDatastore, triggerPreview } = require('./utils');
const { fetchAll, deleteById, find } = require('./queries');
const { getSize, getDurationSeconds } = require('./helpers');

var Video = require('../models/videos');

// Set the video types for validation (uncomment when ready)
//var VIDEO_TYPES = ['video/mp4', 'video/webm', 'video/ogg', 'video/ogv'];

// List Videos
exports.show = function(req, res) {
    fetchAll(function(error, videos) {
        if (error) {
            return res.status(400).send({
                message: error
            });
        }
        res.render('videos', {
            title: 'Videos',
            videos: videos,
            response: (req.response ? req.response : null)
        });
    });
};

exports.deleteVideo = (req, res) => {
    let id = req.body.videoId;
    deleteById(id, (message, video, status) => {
        if (status) {
          return res.status(status).send({
            message
          });
        }
        // send to datastore
        syncData();

        res.redirect('/videos');
    });
}

exports.uploadVideo = function(req, res) { 
    var tempPath = req.file.path;
    var filename = req.file.originalname;
    var mimeType = req.file.mimetype;
    var targetPath = './public/videos/' + filename;
    var src = fs.createReadStream(tempPath);
    var dest = fs.createWriteStream(targetPath);
    //title defaults to filename if none provided
    var title = req.body.title ? req.body.title : filename;
    var duration;
    // check support file types
    //if (VIDEO_TYPES.indexOf(type) == -1) {
    //    return res.status(415).send('Supported video formats: mp4, webm, ogg, ogv');
    //}

    try {
        var processVideo = new ffmpeg(tempPath);
        processVideo.then(function (video) {
            duration = getDurationSeconds(video.metadata.duration.raw);
        }, function (err) {
            console.log('Error: ' + err);
        });
    } catch (e) {
        console.log(e.code);
        console.log(e.msg);
    }

    // write data to file
    src.pipe(dest);

    // Show error
    src.on('error', function(error) {
        if (error) {
            return res.status(500).send({
                message: error
            });
        }
    });

    // Save to database
    src.on('end', function() {
        let host = process.env.EXT_BASE_URL || `${req.protocol}://${req.get('host')}`
        let path = host + '/videos/' + filename;
        let props = {
            title: req.body.title,
            videoName: filename,
            size: getSize(req.file.size),
            mimeType,
            duration,
            path
        }

        var video = new Video(props);
        
        video.save(function(error) {
            if (error) {
                return res.status(400).send({
                    message: error
                });
            }
            // send to datastore
            syncData();
        });
    
        // remove data from temp folder
        fs.unlink(tempPath, function(err) {
            if (err) {
                return res.status(500).send({
                    message: error
                });
            }
            // Redirect to gallery page
            res.redirect('videos');
        });
    });
};

exports.preview = (req, res) => {
    let id = { _id: req.params.id }
    find(id, (err, video) => {
      if (err) {
        console.log(err);
        return res.status(400).send({
            message: err
        });
      }
     
      console.log("VIDEOURL")
      console.log(video.path)
      triggerPreview(video.path, (err, data) => {
        if (err) {
          console.log(err);
          return res.status(400).send({
            message: err
          });
        }
        console.log('preview sucessfully triggered');
        res.redirect('/videos');
      });  
    });
  }

syncData = () => {
    fetchAll((err, data) => {
      if (!err) return postToDatastore(data);
      console.log(err);
    });
}

// Videos authorization middleware
exports.hasAuthorization = function(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
};
