var Playlists = require('../models/playlists');

// Show playlists
exports.show = function (req, res) {
    Playlists.find()
    .sort('-created')
    .populate('videos', ['videoName','duration'])
    .exec(function(error, playlists) {
       if (error) {
           return res.status(400).send({
               message: error
           });
       }
       res.send(playlists);
    });
};

// Create a playlist
exports.create = function (req, res) {
    let playlistItems = (req.body.videos).map((s) => JSON.parse(s));
    let vidIds = playlistItems.map((v) => v.id);

    var playlist = new Playlists({
      location: '/videos',
      name: req.body.name,
      videos: vidIds,
      duration: getDuration(playlistItems)
    });
     
    playlist.save((err, playlist) => {
        if (err) {
            return res.status(400).send({
                message: `Error saving playlist: ${err}`
            });
        } else {
            Playlists.findById(playlist.id)
                .populate('videos', ['videoName','duration'])
                .exec(function(err, popPlaylist) {
                    if (err) {
                        return res.status(400).send({
                            message: err
                        });
                    }
                    res.send(popPlaylist);     
            });
        }
    });

    // TODO: method to post to datastore
    // syncData();
}

// Find playlist by id
exports.findOne = (req,res) => {
    let id = { _id: req.params.id };
    Playlists.findOne(id)
        .exec((err, playlist) => {
            if (err) {
                console.log(err);
                return res.status(400).send({
                    message: error
                });
            }   
            res.send(playlist);
        });
};

//Delete playlist by id
exports.deletePlaylist = function(req, res) {
    let id = req.params.id;
  
    Playlists.findByIdAndRemove(id, (err, playlist) => {
        if(err) {
            console.log(err);
            return res.status(400).send({
                message: err
            });
        }

        // TODO: method to post to datastore
        // syncData();

        res.send({
            message: 'successfully deleted playlist',
            playlist,
          });
    });
}

// Playlists authorization middleware
exports.hasAuthorization = function(req, res, next) {
    if (req.isAuthenticated())
    return next();
    res.redirect('/login');
};


// helper to get full playlist duration
getDuration = (playlistItems) => {
    if (playlistItems.length < 1) {
        return 0
    }
    
    return playlistItems.map((v) => v.duration)
                        .reduce((dur, curr) => dur + curr)
                        .toFixed(2);
}
