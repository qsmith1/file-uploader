// get video model
var Videos = require('../models/videos');

//get all videos
exports.fetchAll = (cb) => {
  Videos.find()
        .sort('-created')
        .exec(cb);
}

//get one video
exports.find = (id, cb) => {
  Videos.findOne(id)
        .exec(cb);
}

//delete a video
exports.deleteById = (id, cb) => {
  
  Videos.findByIdAndRemove(id, (err, data) => {
    if (err) {
      cb(`error finding video with id: ${id}, ${err}`, null, 500)
    } else if (!data) {
      cb(`video with id: ${id} does not exist`, null, 400);
    } else {
      cb('successfully deleted video', data);
    }
  });
}


//save video
exports.save = (props, cb) => {
  const video = new Videos(props);

  video.save((err, data) => {
    if (err) {
      console.log('database error saving video:', err);
      cb('Error saving video', null);
    } else {
      cb(``, data);
    }
  });
}

