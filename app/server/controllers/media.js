var fs = require('fs');
var ffmpeg = require('ffmpeg');
const { postToDatastore, triggerPreview } = require('./utils');
const { fetchAll, find, deleteById, save } = require('./queries');
const { getSize, getDuration } = require('./helpers');

// Set the video types for validation (uncomment when ready)
//var VIDEO_TYPES = ['video/mp4', 'video/webm', 'video/ogg', 'video/ogv'];

// List Videos
exports.show = function(req, res) {
  fetchAll(function(error, videos) {
    if (error) {
      return res.status(400).send({
        message: error
      });    
    }
    
    res.send(videos)
  });
};

// Find video by id
exports.findOne = (req,res) => {
  let id = { _id: req.params.id }
  find(id, (err, video) => {
    if (err) {
      console.log(err);
      return res.status(400).send({
          message: err
      });
    }   
    res.send(video);
  });
};

//Delete video by id
exports.deleteVideo = function(req, res) {
  let id = req.params.id;

  deleteById(id, (message, video, status) => {
    if (status) {
      return res.status(status).send({
        message
      });
    }

    // send to datastore
    syncData();
        
    res.send({
      message,
      video,
    });
  });


}

// Upload video
exports.uploadVideo = function(req, res) {
  var filename = req.file.originalname;
  var tempPath = req.file.path;
  var targetPath = './public/videos/' + filename;
  var src = fs.createReadStream(tempPath);
  var dest = fs.createWriteStream(targetPath);
  var title = req.body.title ? req.body.title : filename;
  var duration;

  // check support file types
  //if (VIDEO_TYPES.indexOf(type) == -1) {
  //    return res.status(415).send('Supported video formats: mp4, webm, ogg, ogv');
  //}

  try {
    var process = new ffmpeg(tempPath);
    process.then(function (video) {
      duration = getDuration(video.metadata.duration.raw);
    }, function (err) {
        console.log('Error: ' + err);
    });
  } catch (e) {
    console.log(e.code);
    console.log(e.msg);
  }
  
  src.pipe(dest);

  // Error handling
  src.on('error', function(error) {
    if (error) {
      return res.status(500).send({
        message: error
      });
    }
  });

  // Save
  src.on('end', function() {
    let path = req.protocol + '://' + req.get('host') + '/videos/' + filename;
    let props = {
      title,
      videoName: filename,
      mimeType: req.file.mimetype,
      size: getSize(req.file.size),
      duration,
      path
    }
    
    save(props, (err, video) => {
      if (err) {
        return res.status(400).send({
          message: err
        });
      }
      syncData();

      // remove from temp folder
      fs.unlink(tempPath, function(err) {
        if (err) {
          return res.status(500).send({
            message: error
          });
        }
      });
      res.send(video);
    });
  });
}

exports.preview = (req, res) => {
  let id = { _id: req.params.id }
  find(id, (err, video) => {
    if (err) {
      console.log(err);
      return res.status(400).send({
          message: err
      });
    }
    
    triggerPreview(video.url, (err, response) => {
      if (err) {
        console.log(err);
        return res.status(400).send({
          message: err
        });
      }
      res.send(response);
    });
    
    
  });
    
}

syncData = () => {
    fetchAll((err, data) => {
      if (!err) return postToDatastore(data);
      console.log(err);
    });
}

