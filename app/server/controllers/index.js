// Show home screen
exports.show = function(req, res) {
	// Render home screen
	res.render('index', {
		title: 'Partouche Media Manager (alpha!)',
		callToAction: 'POC to test file uploading capability'
	});
};
