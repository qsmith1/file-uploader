const axios = require('axios')

const datastoreURL = process.env.DATASTORE_URL || 'localhost:3030'
const variableURL = `${datastoreURL}/sc-datastore/projectData/variable`
const taskURL = `${datastoreURL}/sc-datastore/projectData/task`

exports.postToDatastore = (data) => {
  let items = data.map((item) => {
    return { 
      "key": JSON.stringify(item),
      "value": item.title 
    }
  });
  
   return axios.post(variableURL, {
    jsonrpc: "2.0",
    method: "Variable.UpdateOrCreate",
    params: {
      internalName: "par-content",
      displayName: "Partouche Media",
      projectIdentifier: "par",
      items,
    },
    id: 1
  })
  .then(function(res) {
    console.log(res);
  })
  .catch(function(err) {
    console.log(err);
  }); 
} 

exports.triggerPreview = (path, cb) => {
  return axios.post(taskURL, {
    jsonrpc: "2.0",
    method: "Task.Execute",
    params: {
      softwareInternalName: "partoucheUnityPlayer",
		  taskInternalName: "PlayVideo",
		  taskParams: [
        { "internalName": "path", "value": path, },
        {"internalName": "loop", "value" : "true"}, 
        {"internalName" : "queueMode", "value" : "force"}
      ]
    },
    id: 3
  })
  .then(function(res) {
    console.log(res);
    cb(null, res);
  })
  .catch(function(err) {
    console.log(err);
    cb(err, null);
  }); 
}