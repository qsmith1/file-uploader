// Database URL
module.exports = {
    // Uncomment to connect with MongoDB on Cloud
    //'url' : 'mongodb://mvc-user-connect:opklnm@ds019028.mlab.com:19028/mvc-app'
    'url' : process.env.DB_CONNECTION_STRING || 'mongodb://localhost/mvc-app-multimedia'
};
