const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

// import multer
const multer  = require('multer');
const upload = multer({ dest:'./public/uploads/', limits: {files:1} });

// Import controllers
const index = require('./server/controllers/index');
const auth = require('./server/controllers/auth');
const videos = require('./server/controllers/videos');
const images = require('./server/controllers/images');
const media = require('./server/controllers/media');
const playlists = require('./server/controllers/playlists');

const { seedDatabase } = require('./seed/seed');

// ODM With Mongoose
const mongoose = require('mongoose');
// Modules to store session
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
// Import Passport and Warning flash modules
const passport = require('passport');
const flash = require('connect-flash');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'server/views/pages'));
app.set('view engine', 'ejs');

// Database configuration
const config = require('./server/config/config.js');
mongoose.connect(config.url, { useMongoClient: true });
mongoose.connection.on('error', function() {
	console.error('MongoDB Connection Error. Make sure MongoDB is running.');
});

// Passport configuration
require('./server/config/passport')(passport);


// Middleware
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// Setup public directory
app.use(express.static(path.join(__dirname, 'public')));



// required for passport
// secret for session
app.use(session({
    secret: 'sometextgohere',
    saveUninitialized: true,
    resave: true,
    //store session on MongoDB using express-session + connect mongo
    store: new MongoStore({
        url: config.url,
        collection : 'sessions'
    })
}));

// Init passport authentication
app.use(passport.initialize());
// persistent login sessions
app.use(passport.session());
// flash messages
app.use(flash());




// Routes

app.get('/', index.show);
app.get('/login', auth.signin);
app.post('/login', passport.authenticate('local-login', {
    //Success go to Profile Page / Fail go to login page
    successRedirect : '/profile',
    failureRedirect : '/login',
    failureFlash : true
}));
app.get('/signup', auth.signup);
app.post('/signup', passport.authenticate('local-signup', {
    //Success go to Profile Page / Fail go to Signup page
    successRedirect : '/profile',
    failureRedirect : '/signup',
    failureFlash : true
}));

app.get('/profile', auth.isLoggedIn, auth.profile);
app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

// Setup routes for videos
app.get('/videos', videos.hasAuthorization, videos.show);
app.post('/videos', videos.hasAuthorization, upload.single('video'), videos.uploadVideo);

app.post('/videos/:id', videos.hasAuthorization, videos.deleteVideo);
app.get('/videos/preview/:id', videos.hasAuthorization, videos.preview);

// Setup routes for images
app.post('/images', images.hasAuthorization, upload.single('image'), images.uploadImage);
app.get('/images-gallery', images.hasAuthorization, images.show);

// API routes

/* MEDIA */
// get media list
app.get('/media', media.show);
// upload media
app.post('/media', upload.single('video'), media.uploadVideo);
// get media by id
app.get('/media/:id', media.findOne);
// delete media by id
app.post('/media/:id', media.deleteVideo);
// preview media by id
app.get('/media/preview/:id', media.preview);

/* PLAYLISTS */
// get list of playlists
app.get('/playlists', playlists.show);
// create a playlist
app.post('/playlists', playlists.create);
// find a playlist
app.get('/playlists/:id', playlists.findOne);
// delete a playlist
app.post('/playlists/:id', playlists.deletePlaylist);


// test route for datastore
// retrieve list of assets
app.get('/assets', (req,res) => {

    // query db for assets
    videos.fetchAll((err, items) => {
      if (err) {
        console.log(err);
        return res.status(400).send({
            message: error
        });
      } else {
        let payload = {
            jsonrpc: "2.0",
            method: "Variable.UpdateOrCreate",
            params: {
                internalName: "par-media",
                displayName: "Partouche Media",
                projectIdentifier: "par",
                items,
            },
            id: 1
        }
        res.send(payload);
      }
    });
  });

// Seed Database
app.get('/seed', seedDatabase);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});
