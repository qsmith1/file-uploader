const Video = require('../server/models/videos');
let sampleVideoData = require('./sampleVideoData.json');

// TODO: drop database collection every time seed is called

exports.seedDatabase = function(req, res) {
  sampleVideoData.forEach((obj, err) => {
    var video = new Video(obj); 

    video.save(function(err, data) {
      if (err) {
        console.log('error saving video to database', err);
        res.send('Something went wrong');
      } else {
        console.log('video saved to the database', data);
      }
    });
  });

  res.send('Database seeded');  
};