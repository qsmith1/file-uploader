FROM node:9.1.0-stretch
    RUN apt-get -y update && apt-get install -y wget nano git build-essential yasm pkg-config vim

    # Compile and install ffmpeg from source
    RUN git clone https://github.com/FFmpeg/FFmpeg /root/ffmpeg && \
        cd /root/ffmpeg && \
        ./configure --enable-nonfree --disable-shared --extra-cflags=-I/usr/local/include && \
        make -j8 && make install -j8

    # If you want to add some content to this image because the above takes a LONGGG time to build
    # Setting it to a different value will cause the commands below to be run no matter what
    ARG CACHEBREAK=1

    RUN mkdir -p /usr/src/app
    WORKDIR /usr/src/app
    COPY ./app /usr/src/app
    RUN mkdir -p ./public/videos
    RUN npm install

    EXPOSE 3000
    CMD ["npm", "start"]